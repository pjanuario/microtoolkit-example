var Logger = require('./logger');
var log = Logger.getLogger('micro.client.example');
var ZSSClient = require('zmq-service-suite-client');

var config = {
  // broker frontend address
  broker: process.env.MICRO_BROKER_FE_ADDR || 'tcp://127.0.0.1:7777',
  // service unique identifier
  sid: 'PONG',
  // client identity (optional), defaults to 'client'
  identity: "PONG_CLIENT",
  // client timeout in ms (optional), defaults to 1s
  timeout: 2000
};

log.info(config, "Client Started");

var client = new ZSSClient(config);

setInterval(function(){
  var now = Date.now()
  client.call("ping", now)
    .then(function (message) {
      log.info('Received request');
    });
}, 2000);
