FROM microtoolkit/node:6-alpine

ENV APP_DIR=/app

COPY package.json $APP_DIR/

WORKDIR $APP_DIR

RUN npm install && npm cache clean

USER node

ADD . $APP_DIR
