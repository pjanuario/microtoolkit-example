var zmq = require('zmq')
var _ = require('lodash')
var os = require('os')

var socket = zmq.socket('router')
socket.identity = 'server' + process.pid;
socket.linger = 0
socket.on('message', (envelope, ts) => {
  console.log(socket.identity + ': received from ' + envelope + ' - ' + ts.toString());
  var now = Date.now()
  socket.send([envelope, ts, now, os.hostname()])
})
socket.on('error', (err) => {
  console.log('error received =>', err)
})
socket.bindSync(process.env.MICRO_BROKER_FE_ADDR || 'tcp://0.0.0.0:5555')
