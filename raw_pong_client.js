var zmq = require('zmq')

var socket = zmq.socket('dealer')
socket.identity = 'client' + process.pid;
socket.linger = 0
socket.on('message', (now, ts, hostname) => {
  console.log('%s: received from hostname %s - timestamp %s', socket.identity, hostname, ts)
  var report = {
    svc: ts - now,
    client: Date.now() - ts
  };
  console.log('%s: Took cs:%s ms cr:%s ms', socket.identity, report.svc, report.client);
})
socket.on('error', (err) => {
  console.log('error received =>', err)
})
socket.connect(process.env.MICRO_BROKER_FE_ADDR || 'tcp://0.0.0.0:5555')

setInterval(function(){
  var now = Date.now()
  socket.send(now)
  console.log(socket.identity + ': sending ' + now);
}, 2000)
