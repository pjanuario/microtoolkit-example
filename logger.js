var Logger = require('logger-facade-nodejs');
var LoggerConsolePlugin = require('logger-facade-console-plugin-nodejs');

// this is the default config
var config = {
  level: 'info',
  timeFormat: 'YYYY-MM-DD HH:mm:ss.SSS',
  messageFormat: '%time | %logger::%level - %msg',
  json: true
};

var plugin = new LoggerConsolePlugin(config);
Logger.use(plugin);

module.exports = Logger
