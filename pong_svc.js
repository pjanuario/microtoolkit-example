var ZSSService = require('zmq-service-suite-service');
var Logger = require('./logger');
var log = Logger.getLogger('micro.client.example');
var os = require('os');

var config = {
  // service name
  sid: 'PONG',
  broker: process.env.MICRO_BROKER_BE_ADDR || "tcp://127.0.0.1:7776",
  // service heartbeat interval in ms (optional), defaults to 1s
  heartbeat: 1000
};

log.info(config, "Service Started");

var service = new ZSSService(config);

// it register verb ping
service.addVerb('ping', function (payload, message, callback) {
  callback(null, { ts: Date.now(), hostname: os.hostname() });
});

service.run();
